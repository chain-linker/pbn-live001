---
layout: post
title: "오늘의 이벤트모음337"
toc: true
---

 귀결 6.26~27
 1.업비트 블라스트(14시까지)
 4,3,4,4,1,4,1,1,2,2
 https://www.upbit.com/service_center/notice?id=4318
 비트코인, 이더리움, 리플, NFT 등 다양한 디지털 자산, 역중 거래량 1위 거래소 업비트에서 목하 확인해보세요. No.1 Digital Asset Exchange in Korea, Upbit. Trade various digital assets conveniently and securely including Bitcoin, Ethereum, Ripple, NFT etc.
 www.upbit.com
 ▶투자리그 참가신청(18시까지)
 https://upbit.onelink.me/dH32/0hcorscy?appScheme=%7B%22recommendCode%22%3A%228608A%22%7D
 총 10BTC 규모! 그곳 업비트에서 제호 1회 업비트 투자 메이저리그에 같이 참가해요!
 upbit.onelink.me
 2.해피포인트 진에어(~6.30)
 여기를 눌러 링크를 확인하세요.
 s.ppomppu.co.kr
 3.경기상권진흥원 룰렛(~6.28)
 https://gmr.or.kr/gmr/eventView.do
 경기도시장상권진흥원(경상원) 경기도 골목 구석구석에 돈이 돌도록 모든 방안을 여러분과 아울러 만들어 서민경제가 활성화되도록 최선의 노력을 다하겠습니다.
 gmr.or.kr
 4.라이블리 3주년
 카톡쿠폰+페이코결제
 ▶첫구매 5천쿠폰
 https://m.nhlyvly.com/?reco_id=3595702212@k
 ▶페이코모임
 http://payco.kr/KKwhe?partyId=frrmNP4JEx
 라이블리 3주년 축제!+페이코 추가할인까지
 payco.kr
 5.한국남동발전 카톡추가
 톡에서 참여하기
 https://pf.kakao.com/_sKXlxj
 한국남동발전 감사실에서 운영하는 청렴톡톡입니다.
 pf.kakao.com
 6.스벅TV 구독평
 표발 7.2 메일
 https://www.youtube.com/watch?v=I2yAGKm2Qyo
 →폼제출
 https://ds.fdback.me/r/alPCLQi7e084A
 [스벅tv] <스타벅스 유튜브 구독 이벤트> 안녕하세요! 스벅tv입니다. 익금 설문지는  <스타벅스 유튜브 구독 이벤트>  접수를 위한 설문으로, 향후 이벤트가 완료된 후에 죄다 폐기 예정입니다.  당첨자 확인을 위한 수요 정보를 위불위없이 남겨주세요! 감사합니다. 🎁 스벅tv <스타벅스 유튜브 구독이벤트> 🎁 #event ✅ 관여 기우 2024년 6월 13일(목) ~ 6월 26일(수) / 14일간 ✅ 참관 도리 - STEP 1. Starbucks Korea 유튜브 경로 구독하기 - STEP 2. 성교 영적 댓글란에 미션 댓글 작성하기
 ds.fdback.me
 7.삼성화재 구독평
 공발 6.28일
 →댓글복사
 https://www.youtube.com/watch?v=b6oAsvVNx-0
 →폼제출
 https://form.naver.com/response/83YhDsMK-WicY0OK_k55Mg
 네이버 꼴 설문에 곧 참여해 보세요.
 form.naver.com
 8.몽베스트 퀴즈
 공표 7.3 DM
 https://www.instagram.com/p/C8bhs_Itv15/?hl=ko
 1,109 likes, 1,071 comments - montbest - June 20, 2024: "몽베스트 신규 TVC 런칭 기념 #EVENT 💜 미슐랭 소믈리에가 인정한 몽베스트와 청정+상콤 싹 하는 비주얼 이분과 만났습니다! ITI 사회 식음료 품평회에서 4년 유지 3스타의 영광을 수상한 남다른 물맛💧 티마스터의 블랜딩 노하우로 찾아낸...".
 www.instagram.com
 9.휴롬 체험단
 일일 건강하게 해주는 것, 휴롬
 m.hurom.co.kr
 10.중견기업정보마당 퀴즈
 BBC
 https://tally.so/r/wbk256
 Made with Tally, the simplest way to create forms.
 tally.so
 11.하이무크 팔로우
 발표 7.3일
 →팔로우&댓글 캡처
 https://www.instagram.com/p/C70GLfkyKSi/?utm_source=ig_web_copy_link&igsh=MzRlODBiNWFlZA%3D%3D&img_index=1
 443 likes, 489 comments - hymooc - June 4, 2024: ". 📌HY-MOOC 인스타그램 팔로우 이벤트📌 ✔기간: 2024.6.5.~6.26. ※참여율에 따라 오래도록 가능 ✔참여방법: 1. HY-MOOC 인스타그램 팔로우 사후 팔로우 화면 캡처 2. 밤일 게시글 좋아요 및 ‘참여완료’ 댓글 편성 추후 캡처...".
 www.instagram.com
 →폼제출
 https://form.naver.com/response/Oy_9scCJKralGI2IX9qxbQ
 네이버 모드 설문에 도로 참여해 보세요.
 form.naver.com
 12.안전보건공단 질문
 공표 7.3일
 https://blog.naver.com/koshablog/223485637451
 안녕하세요! 안전보건공단입니다! 2024 산업안전보건의 달을 맞아 「색(色)으로 보는 안전」이라는 주제로 ...
 blog.naver.com
 https://form.naver.com/response/43QBmtw4WQV7VByvLuspLw
 네이버 폼 설문에 얼른 참여해 보세요.
 form.naver.com
 13.여수이야기 구독평
 공표 7.8 개별
 https://blog.naver.com/yeosu_city/223486805668
 [여수이야기 이벤트] 2024 여수시 숏폼 형태 공모전 방송 전하고 🍨시~원~한 빙수 먹자! <이벤트기간&...
 blog.naver.com
 14.엘샵 팔로우
 공표 7.8일
 https://www.instagram.com/p/C8bhYysMGLq/?igsh=bndxazh5b2s0aThq
 1,328 likes, 1,416 comments - lotte.lshop - June 20, 2024: "<FOLLOW EVENT> 헬로우 썸머🌊 🥵무더운 여름을 맞이하여 진행하는 시원시원한 엘샵 신규 팔로우 이벤트!💦 엘샵 계정을 본초 팔로우하는 100명에게 스타벅스 e카드 교환권 1만원권 or 배스킨라빈스 교환권 1만원을 증정합니다🍧...".
 www.instagram.com
 15.안산시 퀴즈
 https://form.naver.com/response/DS6LYo0U0WVYxTNGSFxKwg
 네이버 정형 설문에 곧바로 참여해 보세요.
 form.naver.com
 https://www.facebook.com/story.php?story_fbid=756819219961155&id=100068992516958&mibextid=oFDknk&rdid=5qgyPKzknXot66gH
 안산톡톡 2024년 6월호 발행 6월 TALK QUIZ💝 <안산톡톡>6월호 특집 「혁신과 도약으로 완성될 안산 미리보기」를 시거에 읽고 답해주세요!🔎 QR코드를 스캔하면 식 참예 페이지로 이동!﻿( •͈ᴗ-)ᓂ-ෆ 100명을 추첨해 2만 태양 상당의 기프티콘 증정해 드립니다!😆 🎁참여기간 2024. 5. 30.(목) ~ 2024. 6. 26.(수)...
 www.facebook.com
 16.서울문화포털 뉴스레터
 https://culture.seoul.go.kr/culture/main/main.do
 서울시 문화분야 일괄 지식 제공 사이트 서울문화포털, 문화행사, 영상, 문화정보, 신청참여, 새소식
 culture.seoul.go.kr
 17.폼나게위풍당당 팔로우
 공고 7.3일
 →캡처
 https://www.instagram.com/p/C8a4Lemtd6W/
 541 likes, 389 comments - we_dangdang - June 19, 2024: "#EVENT #폼나게_위품당당 인스타그램 채널이 오픈했습니다! 수하 채널인지 궁금하시다구요? #위품당당 인스타그램 팔로우하고, 앞으로 수하 피드가 올라올지 기대해주세요! 실망시키지 않을거예요😍✨ 하 참여방법에 따라 팔로우 인증 남겨주시면...".
 www.instagram.com
 →폼제출
 https://docs.google.com/forms/d/e/1FAIpQLSesovdirNwqDY_v4uGzBfW4MxVs-2tAUsfkdiNkAICfnGLZpA/viewform
 Couldn’t sign you in The browser you’re using doesn’t support JavaScript, or has JavaScript turned off. To keep your Google Account secure, try signing in on a browser that has JavaScript turned on. Learn more Help Privacy Terms
 docs.google.com
 18.전남교육TV 퀴즈
 미래
 표발 7.3일
 →구독캡처
 https://www.youtube.com/watch?v=PlMbk8f2t2g
 →폼제출
 https://form.naver.com/response/Vq4na0FmGDUZar3fgpWvHA
 네이버 스타일 설문에 어서 참여해 보세요.
 form.naver.com
 19.자이티비 영상공유
 표발 6.27일
 https://www.xi.co.kr/dp_event/?event_seq=5240
 자이TV 이벤트
 www.xi.co.kr
 ▶순천그랜드파크자이
 공발 6.28일
 https://www.xi.co.kr/dp_event/?event_seq=5241
 자이TV 이벤트
 www.xi.co.kr
 20.의정부시 설문조사
 발표 6.28일
 →팔로우
 https://www.instagram.com/p/C8T6VqKBlpn/?utm_source=ig_web_copy_link&igsh=MzRlODBiNWFlZA%3D%3D&img_index=1
 634 likes, 352 comments - uijeongbu_official - June 17, 2024: ". 상상이 현실이 되는 이벤트⭐ "시민의 투표는 콘텐츠가 된다!" 의정부시 SNS에서 보고싶은 콘텐츠를 투표해주시면, 배달의민족 기프티콘을 드립니다! ❤️참여기간: 6월17일(월)~6월26일(수) ❤️참여방법 1) 의정부시 인스타그램...".
 www.instagram.com
 →폼제출
 https://form.naver.com/response/to_X0sIHVbnTm13YJIE9Ow
 네이버 모습 설문에 즉각 참여해 보세요.
 form.naver.com
 21.보배반점 퀴즈
 https://www.instagram.com/p/C8YMNMgSlWa/?img_index=2
 288 likes, 312 comments - bobaebanjum_kr - June 18, 2024: "우리 이름을 맞춰줘!😍 #이벤트 새로운 돈 프렌즈의 이름은 무엇일까요? 다가오는 여름, 시원하고 유용하게 사용할 성명 있는 보배반점X롯데칠성 한정판 굿즈!💛 피크닉 매트와 보냉백 2종을 출시한 기념으로 새로운 두친구 이름을 맞추는 50분에게...".
 www.instagram.com
 22.마드주 홈피리뉴얼
 로그인필요
 https://www.madju.co.kr/post/5?fbclid=PAAaYQDgPfdtDAO9twrFYQFpUsk--njkPEEmbiji6fjtF9-pU_xWOAv8z9XrI_aem_Az-7wBxU9vjPajMai4dAEg
 누구나 쉽게 주방을 바꾸다, 주방가구 전문 플랫폼 [마드주] 싱크대 붙박이장 맞춤가구 전문
 www.madju.co.kr
 https://form.naver.com/response/uM4sOZK4_560JWdAo2fPlw
 네이버 모양 설문에 바로 참여해 보세요.
 form.naver.com
 23.필로소피 퀴즈
 공고 6.28 DM
 https://www.instagram.com/p/C75bzPOPMDq/?igsh=MXg4YmllNnpkZGxsZg%3D%3D
 322 likes, 289 comments - lovephilosophykr - June 6, 2024: "#EVENT 🫧다가오는 여름, 6월 행사 시작합니다🤍 그림 정신 물음표의 제품은 무엇일까요❓ 물음표 흉차 필로소피 제품의 이름을 맞혀주세요! 샴푸 + 바디 + 버블배쓰의 3in1 기능으로 필로소피 베스트셀러 향으로 이루어진 제품의 이름은?...".
 www.instagram.com
 24.한국제지협회 종이의날
 https://paperday.modoo.at/
 종이의 일일 이벤트 페이지입니다.
 paperday.modoo.at
 25.한돈 국돼팀
 공표 7.9일
 https://www.han-don.com/event/index.php/view/328/?type=1
 소비자를 위한 한돈의 모든것
 www.han-don.com
 26.GTV 구독평
 표발 6.30 개별
 →구독&좋아요&댓글 캡처
 https://www.youtube.com/watch?v=m0r5Mi5g_Is
 →폼제출
 https://docs.google.com/forms/d/e/1FAIpQLSeECFwY4SySMAskYyDO69XGe1J0Sh3V5iL6dCX9D4o314Bexw/viewform
 Couldn’t sign you in The browser you’re using doesn’t support JavaScript, or has JavaScript turned [여기여](https://goldfish-inhale.com/life/post-00113.html) off. To keep your Google Account secure, try signing in on a browser that has JavaScript turned on. Learn more Help Privacy Terms
 docs.google.com
 27.TPLAYS 165회
 5
 https://www.t-plays.com/event/detail.do?sno=468
 일상의 행운을 선물하는 믿을 행우 있는 SK텔레콤 직영점
 www.t-plays.com
 28.국립농업박물관 팔로우
 발표 7.1 개별
 →캡처
 https://www.instagram.com/agriculturalmuseum_official/
 팔로워 7,673명, 팔로잉 184명, 게시물 374개 - 국립농업박물관(@agriculturalmuseum_official)님의 Instagram 사진 및 동영상 보기
 www.instagram.com
 →폼제출
 https://docs.google.com/forms/d/e/1FAIpQLSdjeM3JLJ_RYrVdmc8G_YnPM-r2dxJhmjRGa1B_x4m5byY6FQ/viewform
 Couldn’t sign you in The browser you’re using doesn’t support JavaScript, or has JavaScript turned off. To keep your Google Account secure, try signing in on a browser that has JavaScript turned on. Learn more Help Privacy Terms
 docs.google.com
 29.서울시 설문조사2개
 https://minwon.seoul.go.kr/poll/poll.do?method=selectPollList&flag=answer
 진행중인 문제 설문결과 번호 표제 총문항 설문기간 참여하기 2 S-Map 오픈랩 사용자 만족도 설문조사 10 2024.06.17~2024.06.27 참여하기 1 서울시 S-Map(Vritual Seoul) 사용자 만족도 설문조사 10 2024.06.17~2024.06.27 참여하기 처음 이 1 사후 최종점 서울특별시청 04524 서울특별시 중구 세종대로 110 [찾아오시는 길] 전화번호 : 02) 120 © 2015. Seoul Metropolitan Government all rights reserved.
 minwon.seoul.go.kr
 30.남동구 카톡추가
 공발 7.3일
 →팔로우&댓글 캡처
 https://pf.kakao.com/_PKiCu/105672591
 🔥더위🔥로 지친 여러분의 일상에 활력을 더해주는 남동구의 이벤트! #남동구 #카카오톡채널 을 붕집 백장 하면 #즐거운일_잔뜩_생깁니다 [💌 참여방법] 1. 남동구 카톡채널 () 아우 추가하기! 2. 남동구 SNS에 올라온 [남동구 카톡채널 추가하면 즐거운 공작 생김 주의] 이벤트 게시글에 댓글 달기 3. 식례 폼에 댓글 및 팔로우 인증샷 제출하기 [📅기간] 2024. 6. 18.(화) ~ 6. 27.(목) [📌당첨자 발표] 202...
 pf.kakao.com
 →폼제출
 https://form.naver.com/response/nBWMCsRr27ourA-n2cJroA
 네이버 공식 설문에 제대로 참여해 보세요.
 form.naver.com
 31.삼정펄프 퀴즈
 표발 7.5 DM
 https://www.instagram.com/p/C8Td5hqyLmO/?img_index=1
 884 likes, 1,001 comments - sjpulp_com - June 16, 2024: ""4"회에 "0"헌하며 달려온 삼정펄프의 빛나는 "50"년! 총 90분께 푸짐한 선물을 드리는 🙇삼정펄프의 기표 탐색 이벤트!🙇 삼정펄프가 <2024 사회공헌기업대상 취약계층 뒷바라지 면 대상> 수상을 기념하여, 여러분께 감사한 마음을 전하기...".
 www.instagram.com
 32.몽글 퀴즈
 표발 7.2 댓글
 https://www.instagram.com/p/C8CiGkkS3rx/?img_index=1
 608 likes, 749 comments - mongle_com - June 10, 2024: "✨초성 QUIZ EVENT✨ 축하 문화를 바꾸는 신개념 축하 & 선물 플랫폼 몽글의 슬로건은? 초성 힌트를 보고 정답을 맞혀주세요. 📍추가 Hint! 마지막 장의 몽글 극 영상에 정답이 숨어있어요! 👉참여 방법👈 1.몽글 계정...".
 www.instagram.com
 33.환경재단 퀴즈
 표발 6.28 DM
 https://www.instagram.com/p/C8YpIl6uI5N/?hl=ko
 1,364 likes, 1,418 comments - koreagreenfund - June 18, 2024: "#EVENT 인스타그램 팔로우 이벤트🌟 1. @koreagreenfund 인스타그램 팔로우하고 2. 환경재단의 상통 유세 이름(2개)을 댓글로 맞혀주세요! ✅추첨을 통해 50명에게 스타벅스 기프티콘 증정! 1. 🍯🌳🐝🌳힌트: 꿀벌이 살아갈 수...".
 www.instagram.com
 34.privacycampaign 퀴즈
 발표 7.1일
 https://www.instagram.com/p/C8bc5bpMZ8D/?igsh=MXRwOWZtZHA3NTJ2Nw%3D%3D&img_index=1
 1,203 likes, 1,574 comments - privacycampaign - June 20, 2024: "💛QUIZ EVENT OPEN💛 2024 이통사와 함께하는 개인정보보호캠페인 프라이비가 번호 변경하고 개인정보를 지킬 생목숨 있도록 정답을 댓글로 알려주세요! 🥰 퀴즈의 정답을 맞히는 분께는 추첨을 통해 와르르 쏟아지는 경품을 드립니다! 🎉🎁 🚩...".
 www.instagram.com
 35.퐁당 카톡추가
 공고 6.28 개별
 https://pf.kakao.com/_ZwGDK/105497256
 ✨퐁당이랑 카톡 플친하고, "베라 기프티콘" 받아가세요!🎁 ✅ 참녜 재주 01. 이하 링크↓ 클릭 뒤 [카톡 채널 추가] 염절 👉🏻 02. 카톡 채팅방에서 이하 조리 👉🏻[💖채널 수확 식전 참여하기💖] << 클릭 03.아래 링크로 플친 인증샷 업로드하면 끝! 👉🏻 (*이미 플친인 경우도 웰컴 메세지 캡쳐하신 귀결 출석 가능해요!😍) ✅ 참여 때 👉🏻 2024.06.03(월) - 2024.06.27...
 pf.kakao.com
 https://form.naver.com/response/3CgDb7hy5j_N3T0LDf07Wg
 네이버 생김새 설문에 그까짓 참여해 보세요.
 form.naver.com
 36.하만카든 퀴즈
 →팔로우
 https://www.instagram.com/p/C8eGMzVxSA7/?hl=ko&img_index=1
 194 likes, 100 comments - harmankardon_kor - June 21, 2024: "[이벤트] 하만카돈 사고 행사 고급스럽고 우아한 디자인의 고품질 사운드🎶 하만카돈 LUNA의 설여 복판 틀린 것을 골라주세요! 미션을 완료한 참가자 40명을 추첨, ☕쿠폰을 선물로 드립니다. 🎧 미션 1 : 정형 인스타그램...".
 www.instagram.com
 →폼제출
 https://docs.google.com/forms/d/e/1FAIpQLSfKMpAPzKdNvaiqql88V3oLo9GCoMIBeLuqgDd4swx9wu8h0g/viewform
 하만카돈 밤일 💝 Sign in to Google to save your progress. Learn more * Indicates required question 고급스럽고 우아한 디자인의 고품질 사운드🎶 하만카돈 LUNA의 해석 중도 틀린 것을 골라주세요! 미션을 완료한 참가자 40명을 추첨, ☕쿠폰을 선물로 드립니다. 🎧 미션 1 : 스타일 인스타그램 @harmankardon_kor 팔로우 🎧 미션 2 : 프로필 링크 클릭하고 하만카돈 의식 설문지 작성! 응모기간 : 2024.6.21(금) –27(목)까지 당첨발표 : ...
 docs.google.com
 37.HC해피콜 리그램
 표발 7.2 DM
 https://www.instagram.com/p/C8LGydGhKe3/?hl=ko
 328 likes, 308 comments - hc.happycall_official - June 13, 2024: "#이벤트 🎂<경>🎈 HC해피콜 창립25주년 소문내기🎈<축>🎂 우리 HC해피콜 반오십된 거 소문내주실 분들 구함🤍 ( ﾉ ﾟｰﾟ)ﾉ＼(ﾟｰﾟ＼)( ﾉ ﾟｰﾟ)ﾉ＼(ﾟｰﾟ＼) ⠀ 본 게시물을 리그램해주시면 추첨을 통해 푸짐한 경품을 드려요 -...".
 www.instagram.com
 38.하차선언 구독평
 공고 6.28일
 →구독&댓글 캡처
 https://www.youtube.com/watch?v=WG9CDehGmSs
 →폼제출
 https://docs.google.com/forms/d/e/1FAIpQLSdBsDegj61cdGfZnjilFVKhoOLejtXoHBDMaN7HaFqtEQ8T5A/viewform
 Couldn’t sign you in The browser you’re using doesn’t support JavaScript, or has JavaScript turned off. To keep your Google Account secure, try signing in on a browser that has JavaScript turned on. Learn more Help Privacy Terms
 docs.google.com
 ​
 ​
