---
layout: post
title: "집에서 즐기는 카페, 일리 원두캔 클라시코 클래식 로스트 커피 시음기"
toc: true
---


## 멍군이네 참으로 리뷰 - 일리 클라시코 클래식 로스트
 차차로 날이 선선해지니까 따뜻한 커피 경계 잔이 절실하다. 유난히 나른한 주말 늦잠자고 일어났을 때는 더더욱!
 우리집은 드롱기 아이코나 커피머신에 원두를 내려 커피를 만든다. 어제오늘 우리 부부 둘다 주야장천 내려먹던 베트남 원두에 질려서 새로운 원두를 찾고 있었는데, 신랑이 쿠팡에서 일리 커피를 주문해왔다. 틀림없이 커피는 일리! 라는 인식이 있고, 이탈리아산인 만치 향도 품질도 좋을 것만 같아 기대됐다.
 일리 클라시코 클래식 로스트 원두캔
 /
 250g
 원산지 이탈리아, 100% 아라비카 원두
 인터넷으로 12,000원 정도에 구입

 커피가 담긴 은색 원두캔. 캔 위에 적힌 빨간색 일리 로고가 눈에 확 들어온다. 유럽에 가면 경계 거관 건너 일리 카페던데 우리나라는 슬슬 일리 쓰는 카페가 번번이 안보인다. 이렇게 캔으로라도 일리를 만나니 반갑구나!

 유통기한은 22년 5월까지. 정녕 250g의 용량이 들어있다. 원산지는 늘 이탈리아다.
 설명을 보면 제물 보존 및 신선도 유지를 위해 질소가스 충전을 했다고 적혀있는데, 실상 스팸이나 참치캔 따듯 캔뚜껑을 여는 시점 안에서 칙-하는 야망 빠지는 소리가 났다.

 이익 커피향 실화? 캔 뚜껑을 열자마자 기분좋은 커피향이 주방을 확 덮는다. 어느 정취 좋은 카페에 온 듯한 느낌! 실사 향이 그만 보관되어오나보다. 우리 부부가 최근까지 마시던 커피는 오래돼서 향이 매우 날아갔는데.. 이 원두캔 커피는 유럽 커피향을 일절 즉속 품고 있다 +_+ 기대된다 일리커피의 맛!

 시거에 드롱기 포터필터 안에 일리커피를 2T 담뿍 부어주었다. 그다음 산지 기허 되지 않은 커피 매트 위에 올려두고 과시 얼마전에 구입한 템퍼로 원두를 꾹꾹 눌러주었다. 템핑할 때는 팔에 힘을 주고 무게감 있는 템퍼로 꾸욱 눌러줘야 커피향이 좋다고 한다. 그래서 일부러 무게감 있는 알루미늄 템퍼를 구입했다.

 샷 추출 사진! [카페원두](https://reachcattle.com/food/post-00010.html) 익금 때가 가옹 의사 좋다. 누구 커피맛이 날지 기대도 되고 :) 일리커피에서 추출한 샷은 우리가 기존에 이용한 베트남산 원두보다 더더욱 색이 진해보인다.

 나는 통상 아메리카노 한계 잔을 만들 기간 샷글라스 1온즈정도까지 샷을 내린다. 그다음 두 샷을 컵에 붓고 물과 얼음을 넣어 완성한다. 이렇게 만들면 스타벅스 심상성 톨사이즈 아메리카노와 맛이 비슷해진다.

 신랑이 관부 아이스 커피는 유리컵에 경계 서여 만들고, 내가 관시 따뜻한 커피는 가만히 샷 두 잔을 더한층 추출해서 스타벅스 머그에 담았다. 커피를 마신 나중 우리 둘의 반응은 "카페온 줄!"이었다. 딴은 카페에서 마시는 커피랑 맛이 똑같다! 지금까지 다양한 원두를 사봤지만 일리커피는 주인 카페 아메리카노와 유사한 향을 낸다. 대중적인 맛이랄까. 이정도면 텀블러에 담아서 출근길에 홀짝홀짝 마시기도 손색이 없겠다.
 일리커피는 맛, 향, 보관 면에서 만족도가 너무너무 높다. 카페 커피와 비슷한 맛이고 원두캔 상태로 보관할 생령 있어 관리까지 편리하다. 앞으로도 질리기 전까지는 다양한 종류의 일리커피를 마셔볼 듯!
 향좋은 커피 찾는 분들께 일리커피를 추천한다.
 

